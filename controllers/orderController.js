const Order = require('../models/Order');
const User = require('../models/User');
const Product = require('../models/Product');
const auth = require('../auth');


let finalAmount = 0;


// ------Place an order
module.exports.orderProduct = (userData) => {

    return User.findById(userData.id).then(user => {
        return Order.find().then(orders => {

            if (userData.isAdmin == false) {

                if(user.shippingInfo.firstName != "" && 
                   user.shippingInfo.lastName != "" && 
                   user.shippingInfo.mobileNo != "" && 
                   user.shippingInfo.addressLine1 != "" && 
                   user.shippingInfo.city != "" && 
                   user.shippingInfo.state != "" && 
                   user.shippingInfo.country != "") {

                let orderNumber = orders.length + 1;


                let userOrder = [];
                const userCart = user.cart;


                userCart.forEach(cartItem => {

                    let cartItemInfo = {
                        productId: cartItem.productId,
                        quantity: cartItem.quantity

                    }

                    userOrder.push(cartItemInfo)


                    finalAmount += cartItem.subTotal;
                })

                let newOrder = new Order({
                    orderNumber: `SHS-00${orderNumber}`,
                    userId: userData.id,
                    products: userOrder,
                    totalAmount: finalAmount
                })

                return newOrder.save().then((order, error) => {
                    if (error) {
                        return false
                    } else {
                        orderNumber++;
                        finalAmount = 0;



                        return User.findById(userData.id).then(user => {
                            user.cart = [];
                            return user.save().then((user, error) => {

                                if (error) {
                                    return false
                                } else {
                                    return "Thank you! We will keep you updated on your order."
                                }
                            })
                        })

                    }
                })

            } else {
                return "Please complete your shipping info first."
            }

            } else {
                return "Admins cannot cannot process an order."
            }

        })

    })



};


// ------Retrieve all customers - admin only
module.exports.retrieveAllOrders = (isAdminData) => {
    return Order.find({}, { _id: 0, orderNumber: 1 }).then(orders => {

        if (isAdminData) {

            return orders
        } else {
            return "You do not have access to this information."

        }
    })
};


// ------------------------------------------MY STRETCH GOALS:
// ------Fulfill/Cancel an order - admin only
module.exports.processOrder = (reqParams, reqbody, isAdminData) => {
    return Order.findOne({ orderNumber: reqParams.orderNumber }).then(order => {


        const newOrderStatus = { orderStatus: reqbody.orderStatus }
        if (isAdminData) {


            order.orderStatus = newOrderStatus.orderStatus;

            return order.save().then((order, error) => {
                if (error) {
                    return false
                } else {

                    return User.findById(order.userId).then(user => {
                    
                        order.products.forEach(product => {
                            user.toReview.push({ productId: product.productId })
                        })

                        return user.save().then((user, error) => {
                            if (error) {
                                return false
                            } else {
                                return `Order status is ${reqbody.orderStatus}`
                            }
                        })

                    })
                

                }
            })


        } else {
            return "You do not have access to this page."

        }
    })
};