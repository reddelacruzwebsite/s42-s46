const Product = require('../models/Product');
const User = require('../models/User');
const Review = require('../models/Review');
const auth = require('../auth');

// ------Add a product
module.exports.addProduct = (reqBody, userData) => {

    return User.findById(userData.id).then(result => {
        if (userData.isAdmin == false) {
            return "You are not an Admin."
        } else {

            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            });

            return newProduct.save().then((product, error) => {
                if (error) {
                    return false
                } else {
                    return "New product added."
                }
            })
        }
    })
};

// ------Retrieve active products
module.exports.retrieveProducts = (userData) => {

    return User.findById(userData.id).then(user => {

        if (user.isAdmin) {

            return Product.find().then(products => {
                return products
            })
        } else {
            return Product.find({ isActive: true }, { _id: 0, createdOn: 0, __v: 0 }).then(products => {
                return products
            })

        }

    })
};

// ------Retrieve a specific product
module.exports.getAProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result
    })
};

// ------Update a product
module.exports.updateProduct = (reqParams, reqBody, userData) => {

    return User.findById(userData.id).then(result => {
        if (userData.isAdmin == false) {
            return "Only admins are allowed to update the products."

        } else {
            let updatedProduct = {
                isActive: reqBody.isActive,
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            }
            return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((updatedProduct, error) => {
                if (error) {
                    return false
                } else {
                    return true
                }
            })

        }
    })
};



// ------------------------------------------MY STRETCH GOALS:

// ------Add a product review
module.exports.reviewProduct = (reqParams, reqbody, userData) => {



    return Product.findById(reqParams.productId).then(product => {
        if (userData.isAdmin == false) {

            let productReviews = product.reviews

            // let averageRating = 0;
            let productReview = {
                userId: userData.id,
                rating: reqbody.rating,
                feedback: reqbody.feedback,
                ratedOn: new Date()
            }
            // product.averageRating = averageRating;
            product.reviews.push(productReview);

            let averageRatingTotal = 0;
                        if(productReviews.length > 0) {
                            productReviews.forEach(productReview => {
                            averageRatingTotal += productReview.rating;

                        })
                        }


            product.averageRating = averageRatingTotal/productReviews.length;

            return product.save().then((product, error) => {
                if (error) {
                    return false
                } else {


                    return User.findById(userData.id).then(user => {

                        for (let i = 0; i < user.toReview.length; i++) {

                            if (user.toReview[i].productId == reqParams.productId) {
                                user.toReview.splice(i, 1);
                                break;
                            }


                        }
                        return user.save().then((user, error) => {
                            if (error) {
                                return false
                            } else {


                                return "Your review has been added."
                            }
                        })

                    })
                }
            })


        } else {
            return "Only customers can leave a product review."
        }
    })
};