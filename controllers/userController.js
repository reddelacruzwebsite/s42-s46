const User = require('../models/User');
const Order = require('../models/Order');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');


// ------Register a user
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10)
    })

    return User.find({email:reqBody.email}).then(users => {

        if(users.length == 0) {
            return newUser.save().then((user, error) => {
            if (error) {
                return false
            } else {
                return true
            }
        })

        } else {
            return "Email is already exists."
        }
   
    })



};

// ------Log in a user
module.exports.loginUser = (reqBody) => {
    return User.findOne({ email: reqBody.email }).then(result => {
        if (result == null) {
            return false
        } else {

            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordCorrect) {
                return { access: auth.createAccessToken(result) }
            } else {
                return false
            }


        }
    })

}


// ------Retrieve own user details - admin or customer
module.exports.retrieveUserDetails = (userData) => {
    return User.findById(userData.id).then(userDetails => {

        let showUserDetails;

        if (userData.isAdmin) {

            showUserDetails = {
                firstName: userDetails.firstName,
                lastName: userDetails.lastName,
                email: userDetails.email
            }

        } else {

            showUserDetails = {
                firstName: userDetails.firstName,
                lastName: userDetails.lastName,
                email: userDetails.email,
                shippingInfo: userDetails.shippingInfo
            }
        }

        return showUserDetails
    })
};

// ------Retrieve all admins Route - admin only Route
module.exports.retrieveAllAdmins = (userData) => {
    return User.find({ isAdmin: true }, { _id: 0, firstName: 1, lastName: 1, email: 1 }).then(users => {

        if (userData.isAdmin) {

            return users
        } else {
            return "You do not have access to this page."

        }
    })
};


// ------Retrieve all customers - admin only
module.exports.retrieveAllCustomers = (isAdminData) => {
    return User.find({ isAdmin: false }, { _id: 0, firstName: 1, lastName: 1, email: 1 }).then(users => {

        if (isAdminData) {

            return users
        } else {
            return "You are not allowed to view other customers' details."

        }
    })
};

// ------Retrieve customer details - admin only
module.exports.retrieveCustomerDetails = (reqParams, isAdminData) => {
    return User.findById(reqParams.customerId).then(customer => {

        let showCustomerDetails = {
            firstName: customer.firstName,
            lastName: customer.lastName,
            email: customer.email,
            shippingInfo: customer.shippingInfo
        }

        if (isAdminData) {
            return showCustomerDetails
        } else {
            return "You are not allowed to view other customers' details."

        }
    })
};




// ------------------------------------------STRETCH GOALS:

// ------Update user role
module.exports.updateUserRole = (reqParams, reqBody, isAdminData) => {

    let newUserRole = {
        isAdmin: reqBody.isAdmin
    }
    return User.findByIdAndUpdate(reqParams.userId, newUserRole).then(user => {

        if (isAdminData) {
            return true

        } else {
            return "This action is disabled for non-admins."

        }
    })
};


// ------Retrieve customers own orders
module.exports.retrieveCustomerOwnOrders = (userData) => {
    return Order.find({ userId: userData.id }, { _id: 0, userId: 0, __v: 0 }).then(orders => {


        if (userData.isAdmin == false) {
            if (orders.length > 0) {

                return orders
            } else {
                return "You do not have any orders yet."
            }

        } else {
            return "Admins do not have orders."

        }
    })
};


// ------Add to cart 
module.exports.addToCart = (reqparams, reqBody, userData) => {
    return Product.findById(reqparams.productId).then(product => {
        return User.findById(userData.id).then(user => {

            if (userData.isAdmin == false) {

            if(user.cart.length > 0) {
                user.cart.forEach(cartItem => {
                    if(cartItem.productId == reqparams.productId) {
                        cartItem.quantity += reqBody.quantity
                        cartItem.subTotal = cartItem.quantity * cartItem.price
                    } else {
                        const productToBeAdded = {
                        productId: reqparams.productId,
                        productName: product.name,
                        price: product.price,
                        quantity: reqBody.quantity,
                        subTotal: reqBody.quantity * product.price

            }
                 user.cart.push(productToBeAdded)
                    }
                })

            } else {
                const productToBeAdded = {
                productId: reqparams.productId,
                productName: product.name,
                price: product.price,
                quantity: reqBody.quantity,
                subTotal: reqBody.quantity * product.price

  
            }
                 user.cart.push(productToBeAdded)

            }

                return user.save().then((user, error) => {
 
                    if (error) {
                        return false
                    } else {
                        let totalQuantity = 0;
                        let totalAmount = 0;
                        user.cart.forEach(cartItem => {

                            totalQuantity += cartItem.quantity;
                            totalAmount += cartItem.subTotal;

                        })

                        return `Product added to cart. 
                        Items in your cart: ${totalQuantity}
                        Current total: ${totalAmount}`
                    }
                })


            } else {
                return "Admins cannot add to cart."

            }
        })
    })
};


// ------View cart items
module.exports.viewCartItems = (userData) => {

    return User.findById(userData.id).then(user => {
        // return console.log(userData)
        if (userData.isAdmin == false) {

            return user.cart

        } else {
            return "This page is for customers only."

        }
    })

};


// ------Update cart item quantity
module.exports.updateCartItemQty = (reqBody, userData) => {

    return User.findById(userData.id).then(user => {
        // return console.log(userData)
        if (userData.isAdmin == false) {

            const userCart = user.cart
            userCart.forEach(cartItem => {

                if (reqBody.cartId == cartItem._id) {
                    cartItem.quantity = reqBody.quantity
                    cartItem.subTotal = reqBody.quantity * cartItem.price

                }
            })



            return user.save().then((user, error) => {

                if (error) {
                    return false
                } else {
                    return "Cart has been updated."
                }
            })


        } else {
            return "This page is for customers only."

        }
    })

};


// ------Empty Cart
module.exports.clearCart = (userData) => {

    return User.findById(userData.id).then(user => {
        // return console.log(userData)
        if (userData.isAdmin == false) {
            user.cart = []

            return user.save().then((user, error) => {

                if (error) {
                    return false
                } else {
                    return "Your cart is now empty."
                }
            })


        } else {
            return "This page is for customers only."

        }
    })

};




// ------------------------------------------MY STRETCH GOALS:

// ------Update user info - My own stretch goal LOL :D
module.exports.updateUserInfo = (reqBody, userData) => {
 
    return User.findById(userData.id).then(result => {
        if (userData.isAdmin) {

            return User.findById(userData.id).then((user) => {

                if(reqBody.firstName) {
                user.firstName = reqBody.firstName;
                }

                if(reqBody.lastName) {
                user.lastName = reqBody.lastName;
                }

                return user.save().then((user, error) => {

                    if (error) {
                        return false
                    } else {
                        return true
                    }
                })


            }) 

        } else { 

            let customerInfo = {
                firstName: reqBody.firstName,
                lastName: reqBody.lastName
            }
       

            return User.findById(userData.id).then((user) => {
                if(reqBody.firstName) {
                user.firstName = customerInfo.firstName;
                }

                if(reqBody.lastName) {
                user.lastName = customerInfo.lastName;
                }

                if(reqBody.shippingInfo) {
                user.shippingInfo = reqBody.shippingInfo;   
                }                 



                return user.save().then((user, error) => {

                    if (error) {
                        return false
                    } else {
                        return true
                    }
                })


            })

        }

    })
}