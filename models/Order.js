const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    orderNumber: {
        type: String,
        required: [true, "Order number is required"]
    },
    userId: {
        type: String,
        required: [true, "User ID is required"]
    },
    products: [{
        productId: {
            type: String,
            required: [true, "Product ID is required"]
        },
        quantity: {
            type: Number,
            required: [true, "Quantity is required"]
        }
    }],
    totalAmount: {
        type: Number,
        required: [true, "Total amount is required"]
    },
    orderStatus: {
        type: String,
        default: "pending"
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Order", orderSchema);