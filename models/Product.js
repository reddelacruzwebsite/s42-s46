const mongoose = require('mongoose');

// SUPER Hot Sauce - Mild
// 19.99
// Perfect for mild spice level lovers.


// SUPER Hot Sauce - Medium
// 24.99
// Perfect for medium spice level lovers.


// SUPER Hot Sauce - Hot
// 34.99
// Perfect for hot spice level lovers.


// SUPER Hot Sauce - Extreme
// 44.99
// Perfect for extreme spice level lovers.


const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Product name is required"]
    },
    description: {
        type: String,
        required: [true, "Description is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    averageRating: {
        type: Number,
        default: 0
    },
    reviews: [{
            userId: {
                type: String,
                required: [true, "User IDis required"]
            },
            rating: {
                type: Number,
                required: [true, "Rating is required"]
            },
            feedback: {
                type: String,
                required: [true, "Feedback is required"]
            },
            ratedOn: {
                type: Date,
                required: [true, "Date is required"]
            }
        }

    ]


})

module.exports = mongoose.model("Product", productSchema);