const mongoose = require('mongoose');

const reviewSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, "User IDis required"]
    },
    productId: {
        type: String,
        required: [true, "Product ID is required"]
    },
    rating: {
        type: Number,
        required: [true, "Rating is required"]
    },
    feedback: {
        type: String,
        required: [true, "Feedback is required"]
    },
    ratedOn: {
        type: Date,
        default: new Date()
    }

})

module.exports = mongoose.model("Review", reviewSchema);