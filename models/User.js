const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        default: ""
    },
    lastName: {
        type: String,
        default: ""
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    shippingInfo: { 
        firstName: {
            type: String,
            default: ""
        },
        lastName: {
            type: String,
            default: ""
        },
        mobileNo: {
            type: String,
            default: ""
        },
        addressLine1: {
            type: String,
            default: ""
        },
        addressLine2: {
            type: String,
            default: ""
        },
        city: {
            type: String,
            default: ""
        },
        state: {
            type: String,
            default: ""
        },
        country: {
            type: String,
            default: ""
        }
    },
    cart: [{
        productId: {
            type: String,
            required: [true, "Product ID is required"]
        },
        productName: {
            type: String,
            required: [true, "Product name is required"]
        },
        price: {
            type: Number,
            required: [true, "Product price is required"]
        },
        quantity: {
            type: Number,
            required: [true, "Quantity is required"]
        },
        subTotal: {
            type: Number,
            required: [true, "Sub total is required"]
        }
    }],
    toReview: [{
        productId: {
            type: String,
            required: [true, "Product ID is required"]
        }
    }]

})


module.exports = mongoose.model("User", userSchema);