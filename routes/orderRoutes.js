const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController')
const auth = require('../auth')


// ------Place an order Route
router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    orderController.orderProduct(userData).then(resultFromController => res.send(resultFromController))
})

// ------Retrieve all orders Route - admin only
router.get("/", auth.verify, (req, res) => {

    const isAdminData = auth.decode(req.headers.authorization).isAdmin

    orderController.retrieveAllOrders(isAdminData).then(resultFromController => res.send(resultFromController))
})




// ------------------------------------------MY STRETCH GOALS:

// ------Fulfill/Cancel an order Route - admin only
router.put("/:orderNumber", auth.verify, (req, res) => {

    const isAdminData = auth.decode(req.headers.authorization).isAdmin

    orderController.processOrder(req.params, req.body, isAdminData).then(resultFromController => res.send(resultFromController))
})



module.exports = router;