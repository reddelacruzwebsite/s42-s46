const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController')
const auth = require('../auth')

// ------Add a product Route
router.post("/", auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);
    userData = {
        id: userData.id,
        isAdmin: userData.isAdmin
    }

    productController.addProduct(req.body, userData).then(resultFromController => res.send(resultFromController))
});


// ------Retrieve products Route
router.get("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    productController.retrieveProducts(userData).then(resultFromController => res.send(resultFromController))

})


// ------Retrieve a specific product Route
router.get("/:productId", (req, res) => {
    productController.getAProduct(req.params).then(resultFromController => res.send(resultFromController))
})


// ------Update product Route
router.put("/:productId", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    productController.updateProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController))
})



// ------------------------------------------MY STRETCH GOALS:

// ------Add a product review Route
router.put("/:productId/review", auth.verify, (req, res) => {

    let userData = auth.decode(req.headers.authorization);


    productController.reviewProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController))
})



module.exports = router;