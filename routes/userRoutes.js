const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController')
const auth = require('../auth')


// ------Register a user Route
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


// ------Register a user Route
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// ------Retrieve own user details (admin or customer) Route
router.get("/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    userController.retrieveUserDetails(userData).then(resultFromController => res.send(resultFromController))
})


// ------Retrieve all admins Route - admin only Route
router.get("/admins", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    userController.retrieveAllAdmins(userData).then(resultFromController => res.send(resultFromController))
})


// ------Retrieve all customers - admin only Route
router.get("/customers", auth.verify, (req, res) => {

    const isAdminData = auth.decode(req.headers.authorization).isAdmin

    userController.retrieveAllCustomers(isAdminData).then(resultFromController => res.send(resultFromController))
})


// ------Retrieve customer details - admin only Route
router.get("/customers/:customerId", auth.verify, (req, res) => {

    const isAdminData = auth.decode(req.headers.authorization).isAdmin

    userController.retrieveCustomerDetails(req.params, isAdminData).then(resultFromController => res.send(resultFromController))
})







// ------------------------------------------STRETCH GOALS:

// ------Update user role Route
router.put("/:userId/updateRole", auth.verify, (req, res) => {

    const isAdminData = auth.decode(req.headers.authorization).isAdmin

    userController.updateUserRole(req.params, req.body, isAdminData).then(resultFromController => res.send(resultFromController))
})


// ------Retrieve customers own orders Route
router.get("/orders", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    userController.retrieveCustomerOwnOrders(userData).then(resultFromController => res.send(resultFromController))
})


// ------Add to cart Route
router.put("/addToCart/:productId", auth.verify, (req, res) => {

    let userData = auth.decode(req.headers.authorization)

    userController.addToCart(req.params, req.body, userData).then(resultFromController => res.send(resultFromController))
})


// ------View cart Route
router.get("/cart", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    userController.viewCartItems(userData).then(resultFromController => res.send(resultFromController))
})

// ------Change cart item quantity Route
router.put("/cart", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    userController.updateCartItemQty(req.body, userData).then(resultFromController => res.send(resultFromController))
})

// ------Empty cart Route
router.put("/cart/clear", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    userController.clearCart(userData).then(resultFromController => res.send(resultFromController))
})



// ------------------------------------------MY STRETCH GOALS:

// ------Update user info - my own stretch goal Route
router.put("/updateUserInfo", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    userController.updateUserInfo(req.body, userData).then(resultFromController => res.send(resultFromController))
})




module.exports = router;